#! /usr/bin/env python3

#Simple stress monitor. When given input will state based on message is stress is present (y/n)
#
#Download following packages by
    #  pip install numpy
    # pip install --user -U ntlk
    # pip install pandas
    # pip install matplotlib
    # pip3 install -U scikit-learn

import pandas as pd
import numpy as np
import nltk
import re
import matplotlib.pyplot as plt
from matplotlib import *
import string
from nltk.corpus import stopwords

def main():
    print('Hello! I am SAM. Nice to meet you!')
    print('Running startup sequence...')
    data = pd.read_csv("stress.csv")
    print(data.head())
    #In python console checked for null values in the data set. There were none.
    #Stopwords
    nltk.download('stopwords')
    stemmer = nltk.SnowballStemmer("english")
    stopword_set = set(stopwords.words('english'))
    #-----------------------------------------------------------------------------------------------------------------------
    Work = input('Would you like to see me print the text as I process it? y for yes, n for no: ')
    if Work =='y':
        verbose = True
    else:
        verbose = False
    print('Processing...')
    data["text"] = data["text"].apply(cleantext, args=(stopword_set, stemmer, verbose,))
    print('I am complete with the data job!')

    DataSorting(data)
    DataOutput(data)
    print("I have done all I can. I hope it helped! Bye! またね!")



def DataSorting(data):
#This is the stress level, it must be a 1 or 0
    data["label_descriptive"] = data["label"].map({0: "No_Stress",1: "Stress"})
#This is the confidence level it's between 0.4 and 1. When something is said with higher confidence normally means higher risk
    data["conf_descriptive"] = data["confidence"].map({0.4:"Low_Con",
                                                 0.5:"Low_Con",
                                                 0.571428571:"Low_Con",
                                                 0.6:"Med_Con",
                                                 0.666666667:"Med_Con",
                                                 0.7:"Med_Con",
                                                 0.8:"High_Con",
                                                 0.833333333:"High_Con",
                                                 1:"VeryHigh_Con"})
def DataOutput(data):
#Here are the diffrent outputs for the If statements
    DataOut1 = data[["label_descriptive", "conf_descriptive"]]
    DataOut2 = data[["text","label_descriptive", "conf_descriptive"]]
# Here is an If statement so that Sample Text and Everything else dosent always need to print when running tests. It drives me crazy. I also thought it brought some charm to SAM.
    #data["id"] = data["id"].map({"id#"})
    input1 = input ('Would you like for me to print a text sample as well as your data? y for yes, n for no: ')
    if input1 =='y':
        print(DataOut2)
    else:
        print('Proccessing...')
    input2 = input('Would you like for me to download a scatter plot with Stress over Confidence? y for yes, n for no: ')
    if input2 =='y':
        df = pd.DataFrame(data, columns=["label", "confidence"])
        #print('=======================')
        #print(df)
#        print('=======================')
        Graph1 = df.plot.scatter(x="label", y="confidence");
        for ftype in ('pdf', 'png'):
            fname = f'FullDataSetScatterPlot.{ftype}'
            plt.savefig(fname)
            print(f'# saved plot to file {fname}')
    else:
        print('Proccessing...')

    input3 =  input('Would you like for me to download a bar graph with Stress and Confidence? y for yes, n for no: ')
    if input3 =='y':
        df = pd.DataFrame(data, columns=["label", "confidence"])
        Graph2 = df.plot.bar(x='label', y='confidence', rot=0)
        for ftype in ('pdf', 'png'):
            fname = f'FullDataSetBarGraph.{ftype}'
            plt.savefig(fname)
            print(f'# saved plot to file {fname}')
    else:
        print('Proccessing...')

    print('Printing data table...')
    print(DataOut1)

    print('Done!')
def cleantext(text, stopword_set, stemmer, verbose):
    """Cleans text of special characters"""
    orig_text = text
    text = str(text).lower()
    text = re.sub('\[.*?\]', '', text)
    text = re.sub('https?://\S+|www\.\S+', '', text)
    text = re.sub('<.*?>+', '', text)
    text = re.sub('[%s]' % re.escape(string.punctuation), '', text)
    text = re.sub('\n', '', text)
    text = re.sub('\w*\d\w*', '', text)
    text = [word for word in text.split(' ') if word not in stopword_set]
    text=" ".join(text)
    text = [stemmer.stem(word) for word in text.split(' ')]
    text=" ".join(text)
    if verbose:
        print('==============================')
        print(text)
        print('******************************')
    return(text)
main()

TextN = str(text)
print(TextN)
#f = open('CleanedStress.csv')
#f.write(TextN)
#f.close()
print('Wrote Clean Text to a file')
